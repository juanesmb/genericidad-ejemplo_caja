/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Util.Conjunto;

/**
 *
 * @author Juan Moncada
 */
public class TestNuevoConjunto {
    
    public static void main(String[] args) {
        Conjunto<Integer> c1=new Conjunto(9);
        Conjunto<Integer> c2=new Conjunto(10);
        Conjunto<Integer> c3=new Conjunto(30);
        
        try{
            
            c1.adicionarElemento(10);
            c1.adicionarElemento(9);
            c1.adicionarElemento(8);
            c1.adicionarElemento(3);
            c1.adicionarElemento(5);
            c1.adicionarElemento(1);
            c1.adicionarElemento(4);
            c1.adicionarElemento(6);
            c1.adicionarElemento(7);
            
            c2.adicionarElemento(5);
            c2.adicionarElemento(1);
            c2.adicionarElemento(6);
            c2.adicionarElemento(4);
            c2.adicionarElemento(0);
            c2.adicionarElemento(10);
            c2.adicionarElemento(3);
            c2.adicionarElemento(16);
            c2.adicionarElemento(85);
            c2.adicionarElemento(100);
            
            
            System.out.print(c1.toString() + "conjunto C1 inicial");
            System.out.println("");
            //System.out.print(c2.toString() + "conjunto C2 inicial");
            System.out.println("");

            //probar todos los métodos públicos(imprimir)
            //Recomendación: Realicen método para crear cada una de las pruebas.
            probarSelectionSort(c1);
            probarSelectionSort(c2);
            System.out.println("");
            //probarOrdenarBurbuja(c1);
            System.out.println("");
            //probarRemoverUnElemento(c1,7);
            System.out.println("");
            //probarConcatenar(c1,c2);
            System.out.println("");
            //probarConcatenarRestrictivo(c1,c2);
            System.out.println("");
        }catch(Exception e){
            System.err.println(e.getMessage());
        }
    }

    private static void probarOrdenarBurbuja(Conjunto<Integer> c1) throws Exception{
            c1.ordenarBurbuja();
            System.out.println(c1.toString());
    }      

    private static void probarRemoverUnElemento(Conjunto<Integer> c, int elemento) throws Exception{
        
        c.remover(elemento);
        System.out.println("");
        for (int i = 0; i < c.getLength(); i++) 
            System.out.print(c.getCajas()[i].getObjeto() + " ");
        
    }

    private static void probarConcatenar(Conjunto<Integer> c1, Conjunto<Integer> c2) {
        c1.concatenar(c2);
        System.out.println("cadena c1 y c2 concatenadas" + "\n");
        System.out.println(c1.toString());
    }

    private static void probarConcatenarRestrictivo(Conjunto<Integer> c1, Conjunto<Integer> c2) throws Exception {
        c1.concatenarRestrictivo(c2);
        System.out.println("cadena c1 y c2 concatenadas" + "\n");
        System.out.println(c1.toString());
    }

    private static void probarSelectionSort(Conjunto<Integer> c1) throws Exception{
        c1.ordenar();
        System.out.println(c1.toString());
    }
}
