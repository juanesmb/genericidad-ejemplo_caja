/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.util.Arrays;

/**
 * Clase contenedora: Cada uno de sus elementos con cajas parametrizadas
 * Conjunto es una estructura da datos estática
 *
 * @author MADARME
 */
public class Conjunto<T> {

    //Estructura de datos estática
    private Caja<T>[] cajas;
    private int i = 0;

    public Conjunto() {
    }

    public Conjunto(int cantidadCajas) {
        if (cantidadCajas <= 0) {
            throw new RuntimeException("No se pueden crear el Conjunto");
        }

        this.cajas = new Caja[cantidadCajas];
    }

    public void adicionarElemento(T nuevo) throws Exception {
        if (i >= this.cajas.length) {
            throw new Exception("No hay espacio en el Conjunto");
        }

        if (this.existeElemento(nuevo)) {
            throw new Exception("No se puede realizar inserción, elemento repetido");
        }

        this.cajas[i] = new Caja(nuevo);
        this.i++;

    }

    public T get(int indice) {
        if (indice < 0 || indice >= this.getLength()) {
            throw new RuntimeException("Índice fuera de rango");
        }

        return this.cajas[indice].getObjeto();

    }

    public int indexOf(T objBuscar) {

        for (int j = 0; j < i; j++) {

            //Sacando el estudiante de la caja:
            T x = this.cajas[j].getObjeto();

            if (x.equals(objBuscar)) {
                return j;
            }
        }

        return -1;

    }

    public void set(int indice, T nuevo) {
        if (indice < 0 || indice >= this.getLength()) {
            throw new RuntimeException("Índice fuera de rango");
        }

        this.cajas[indice].setObjeto(nuevo);

    }

    public boolean existeElemento(T nuevo) {

        //Sólo estoy comparando por los estudiantes matriculados
        for (int j = 0; j < i; j++) {

            //Sacando el estudiante de la caja:
            T x = this.cajas[j].getObjeto();

            if (x.equals(nuevo)) {
                return true;
            }
        }
        return false;
    }

    /**
     * para el grupo A--> Selección para el grupo C--> Inserción
     *
     */
    public void ordenar() throws Exception{
        if (this.cajas == null) {
            throw new RuntimeException("No se puede ordenar, el conjunto está vacío");
        }
        int rta_compareTo;
        T aux;
        Comparable comparador;
        
        for (int i = 0; i < this.cajas.length; i++) {
            for (int j = i+1; j < this.cajas.length; j++) {
                comparador = (Comparable)this.cajas[j].getObjeto();
                rta_compareTo = comparador.compareTo(this.cajas[i].getObjeto());
                if (rta_compareTo == 0) 
                        throw new Exception("Obj1 es igual a Obj2");
                if(rta_compareTo < 0){
                    aux = this.cajas[i].getObjeto();
                    this.cajas[i].setObjeto(this.cajas[j].getObjeto());
                    this.cajas[j].setObjeto(aux);
                    
                }
            }
        }
            

    }

    /**
     * Realiza el ordenamiento por burbuja
     */
    public void ordenarBurbuja() throws Exception {
        if (this.cajas == null) {
            throw new RuntimeException("No se puede ordenar, el conjunto está vacío");
        }
        int rta_compareTo;
        Comparable comparador;
        T dato_A_comparar;
        Caja<T> aux;
        boolean cambios = false;

        while (true) 
        {
            cambios = false;
            for (int j = 1; j < this.cajas.length; j++) 
            {
                if (this.cajas[j] != null) 
                {
                    comparador = (Comparable) this.cajas[j-1].getObjeto();
                    dato_A_comparar = this.cajas[j].getObjeto();
                    rta_compareTo = comparador.compareTo(dato_A_comparar);
                    if (rta_compareTo == 0) 
                        throw new Exception("Obj1 es igual a Obj2");
                    else 
                    {
                        if (rta_compareTo > 0) 
                        {
                            //System.out.println("Obj1 > Obj2");
                            aux = this.cajas[j-1];
                            this.cajas[j-1] = this.cajas[j];
                            this.cajas[j] = aux;
                            cambios = true;
                        }
                    }
                }
            }
            if(cambios == false)
                break;
        }
    }

    /**
     * Elimina un elemento del conjunto y compacta
     *
     * @param objBorrado es el objeto que deseo eliminar
     */
    public void remover(T objBorrado) throws Exception{
        
        int i = indexOf(objBorrado);
        if(i == -1)
            throw new Exception("índice fuera de rango");
        
        for (int j = i; j < this.cajas.length; j++) 
        {
            if(j+1<this.cajas.length && this.cajas[j+1] != null)
            {
                this.cajas[j].setObjeto(this.cajas[j+1].getObjeto());
                this.cajas[j+1].setObjeto(null);
            }
            if(j+1==this.cajas.length)
                this.cajas[j].setObjeto(null);
        }
        this.i--;
    }

    /**
     * El método adiciona todos los elementos de nuevo en el conjunto
     * original(this) y el nuevo queda vacío. En este proceso no se toma en
     * cuenta los datos repetidos Ejemplo: conjunto1=<1,2,3,5,6> y
     * conjunto2=<9,1,8>
     * conjunto1.concatenar(conjunto2) da como resultado:
     * conjunto1=<1,2,3,5,6,9,1,8> y conjunto2=null
     *
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     */
    public void concatenar(Conjunto<T> nuevo) {
        
        Caja<T> [] aux = new Caja[this.cajas.length];
        for (int j = 0; j < aux.length; j++) {
            aux[j] = this.cajas[j];
        }
        
        int tamaño  = aux.length+nuevo.getLength();
        this.cajas = new Caja[tamaño];
        
        
        for (int j = 0; j < aux.length; j++) {
            this.cajas[j] = aux[j];
        }
        
        int i = 0;
        for (int j = aux.length; j < this.cajas.length; j++) {
            this.cajas[j] = nuevo.cajas[i];
            i++;
        }
        
        nuevo.removeAll();

    }

    /**
     * El método adiciona todos los elementos de nuevo en el conjunto
     * original(this) y el nuevo queda vacío. En este proceso SI toma en cuenta
     * los datos repetidos Ejemplo: conjunto1=<1,2,3,5,6> y conjunto2=<9,1,8>
     * conjunto1.concatenar(conjunto2) da como resultado:
     * conjunto1=<1,2,3,5,6,9,8> y conjunto2=null
     *
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     */
    public void concatenarRestrictivo(Conjunto<T> nuevo) throws Exception{
        
        for (int j = 0; j < this.cajas.length; j++) 
        {
            T objeto = this.cajas[j].getObjeto();
            if(nuevo.existeElemento(objeto))
                nuevo.remover(objeto);
        }
        
        Caja<T> [] aux = new Caja[this.cajas.length];
        for (int j = 0; j < aux.length; j++) {
            aux[j] = this.cajas[j];
        }
        
        int tamaño  = aux.length+nuevo.getCapacidad();
        this.cajas = new Caja[tamaño];
        
        
        for (int j = 0; j < aux.length; j++) {
            this.cajas[j] = aux[j];
        }
        
        int i = 0;
        for (int j = aux.length; j < this.cajas.length && nuevo.getCajas()[i].getObjeto() != null; j++) {
            this.cajas[j] = nuevo.cajas[i];
            i++;
        }
        
        nuevo.removeAll();

    }

    public void removeAll() {
        this.cajas = null;
        this.i = 0;
    }

    @Override
    public String toString() {
        String msg = "\n" + "******** CONJUNTO*********\n";

        /*for (int j = 0; j < this.cajas.length; j++) {
             msg += this.cajas[j].getObjeto().toString() + "\n";
        }*/
        for (Caja c : this.cajas) {
            msg = msg + c.getObjeto().toString() + "\n";
        }

        return msg;
    }

    /**
     * Obtiene la cantidad de elementos almacenados
     *
     * @return retorna un entero con la cantidad de elementos
     */
    public int getCapacidad() {
        return this.i;
    }

    /**
     * Obtiene el tamaño máximo de cajas dentro del Conjunto
     *
     * @return int con la cantidad de cajas
     */
    public int getLength() {
        return this.cajas.length;
    }

    /**
     * Obtiene el mayor elemento del Conjunto, recordar que el conjunto no posee
     * elementos repetidos.
     *
     * @return el elemnto mayor de la colección
     */
    public T getMayorElemento() {
        if (this.cajas == null) {
            throw new RuntimeException("No se puede encontrar elemento mayor, el conjunto está vacío");
        }

        T mayor = this.cajas[0].getObjeto();
        for (int i = 1; i < this.getCapacidad(); i++) {
            //Utilizo la interfaz comparable y después su método compareTo
            Comparable comparador = (Comparable) mayor;
            T dato_A_comparar = this.cajas[i].getObjeto();
            // Resta entra mayor-datoAComparar 
            int rta_compareTo = comparador.compareTo(dato_A_comparar);
            if (rta_compareTo < 0) {
                mayor = dato_A_comparar;
            }
        }
        return mayor;
    }

    public Caja<T>[] getCajas() {
        return cajas;
    }
    
    

}
